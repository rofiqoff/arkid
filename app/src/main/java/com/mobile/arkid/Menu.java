package com.mobile.arkid;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class Menu extends AppCompatActivity {

    public MediaPlayer backSound;
    Dialog dialog;
    ToggleButton toggleButton;
    Button pengaturan, tentang, bantuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ImageView belajar = (ImageView) findViewById(R.id.btBelajar);
        ImageView bermain = (ImageView) findViewById(R.id.btPlay);
        ImageView keluar = (ImageView) findViewById(R.id.btClose);
        pengaturan = (Button) findViewById(R.id.btmenu);

        backSound = MediaPlayer.create(this, R.raw.sound1);
        backSound.setLooping(true); //perulangan sound
        backSound.setVolume(1, 1); //setting volume sound
        backSound.start(); //memulai sound

        pengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pengaturan();
            }
        });

        belajar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, Belajar.class);
                startActivity(intent);
            }
        });

        bermain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, LevelGame.class);
                startActivity(intent);

            }
        });

        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Anda Keluar dari Aplikasi", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    public void pengaturan() {
        dialog = new Dialog(Menu.this);
        dialog.setContentView(R.layout.setting);
        dialog.setTitle(R.string.text_pengaturan);
        dialog.setCancelable(true);
        dialog.show();

        toggleButton = (ToggleButton) dialog.findViewById(R.id.btsuara);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleClicked(v);
                //dialog.dismiss();
            }
        });

        tentang = (Button) dialog.findViewById(R.id.bttentang);
        tentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Tentang.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        bantuan = (Button) dialog.findViewById(R.id.btbantuan);
        bantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Bantuan.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }

    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            backSound.setVolume(1, 1);
        } else {
            backSound.setVolume(0, 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        backSound.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        backSound.start();
    }

    @Override
    public void onBackPressed() {
        backSound.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backSound.stop();
    }

}

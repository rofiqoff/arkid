package com.mobile.arkid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class LevelGame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_game);

        ImageView level1 = (ImageView)findViewById(R.id.btlevel1);
        ImageView level2 = (ImageView)findViewById(R.id.btlevel2);
        ImageView level3 = (ImageView)findViewById(R.id.btlevel3);
        Button highscore = (Button)findViewById(R.id.bthighscore);

        level1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LevelGame.this, Level1.class);
                startActivity(intent);
            }
        });

        level2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LevelGame.this, Level2.class);
                startActivity(intent);
            }
        });

        level3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LevelGame.this, Level3.class);
                startActivity(intent);
            }
        });

        highscore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LevelGame.this, LihatData.class);
                startActivity(intent);
            }
        });
    }
}

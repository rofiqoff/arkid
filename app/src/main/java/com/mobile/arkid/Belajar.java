package com.mobile.arkid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class Belajar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_belajar);

        Button jml = (Button) findViewById(R.id.btPenjumlahan);
        Button krg = (Button) findViewById(R.id.btPengurangan);
        Button kali = (Button) findViewById(R.id.btPerkalian);
        Button bagi = (Button) findViewById(R.id.btPembagian);

        jml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Belajar.this, BelajarPenjumlahan.class);
                startActivity(intent);
            }
        });

        krg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Belajar.this, BelajarPengurangan.class);
                startActivity(intent);
            }
        });

        kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Belajar.this, BelajarPerkalian.class);
                startActivity(intent);
            }
        });

        bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Belajar.this, BelajarPembagian.class);
                startActivity(intent);
            }
        });
    }
}

package com.mobile.arkid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class BelajarPerkalian extends AppCompatActivity {

    int count=0;
    int max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_belajar_perkalian);

        final int image[] = {R.drawable.kali1, R.drawable.kali2, R.drawable.kali03, R.drawable.kali4, R.drawable.kali5};
        max = image.length;

        Button prev = (Button)findViewById(R.id.btprev);
        Button next = (Button)findViewById(R.id.btnext);
        final ImageView materi = (ImageView) findViewById(R.id.imateri);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i;
                count--;
                if (count<0){
                    count=max-1;
                }else if (count>=max){
                    count=0;
                }
                i=count;
                materi.setImageResource(image[i]);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i;
                count++;
                if (count<0){
                    count=max-1;
                }else if(count>=max) {
                    count=0;
                }
                i = count;
                materi.setImageResource(image[i]);
            }
        });
    }
}

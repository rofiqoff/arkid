package com.mobile.arkid;

/**
 * Created by nur on 11/26/2016.
 */
public class config {
    //Alamat URL tempat kita meletakkan script PHP di PC Server
    // Link untuk INSERT Data
    public static final String URL_ADD="http://arkidgame.esy.es/create.php";
    public static final String URL_GET_ALL="http://arkidgame.esy.es/read.php";

    // Filed yang digunakan untuk dikirimkan ke Database, sesuaikan saja dengan Field di Tabel Mahasiswa
    public static final String KEY_EMP_ID = "id";
    public static final String KEY_EMP_NAMA = "nama";
    public static final String KEY_EMP_SKOR = "skor";

    // Tags Format JSON
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id";
    public static final String TAG_NAMA = "nama";
    public static final String TAG_SKOR = "skor";
}

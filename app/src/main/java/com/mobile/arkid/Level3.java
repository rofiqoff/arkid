package com.mobile.arkid;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Level3 extends AppCompatActivity {
    private CountDownTimer countDownTimer;
    private boolean timerStart = true;
    private final long startTime = 60 * 1000;
    private final long interval = 1 * 1000;
    Dialog dialog;

    TextView angkapertama, angkakedua, angkaketiga, operat, operat2, timer, skor;
    Button pilihan1, pilihan2, pilihan3, pilihan4;
    int angka1;
    int angka2, angka3;
    String st_angka1, nama, hasilskor;
    String st_angka2, st_angka3;
    String oper, oper2;
    int total = 0, score, hasilku;

    void acak() {
        String opp[] = {"+", "-", "*"};
        int operator = (int) (Math.random() * 3);
        oper = opp[operator];
        angka1 = (int) (Math.random() * 10) + 1;
        angka2 = (int) (Math.random() * 10) + 1;

        if (oper.equals(":")) {
            angka1 += angka2;
            int hasilbagi = angka1 % angka2;
            if (hasilbagi != 0) {
                int an1 = angka2 - hasilbagi;
                angka1 = angka1 + an1;
            }
        }

        String opp2[] = {"+", "-"};
        int operator2 = (int) (Math.random() * 2);
        oper2 = opp2[operator2];
        angka3 = (int) (Math.random() * 10) + 1;

        st_angka1 = String.valueOf(angka1);
        angkapertama.setText(st_angka1);

        operat.setText(oper);

        st_angka2 = String.valueOf(angka2);
        angkakedua.setText(st_angka2);

        operat2.setText(oper2);

        st_angka3 = String.valueOf(angka3);
        angkaketiga.setText(st_angka3);

        if (oper.equals("+") && oper2.equals("+")) {
            hasilku = (angka1 + angka2) + angka3;
        } else if (oper.equals("+") && oper2.equals("-")) {
            hasilku = (angka1 + angka2) - angka3;
        } else if (oper.equals("-") && oper2.equals("+")) {
            hasilku = (angka1 - angka2) + angka3;
        } else if (oper.equals("-") && oper2.equals("-")) {
            hasilku = (angka1 - angka2) - angka3;
        } else if (oper.equals("*") && oper2.equals("+")) {
            hasilku = (angka1 * angka2) + angka3;
        } else if (oper.equals("*") && oper2.equals("-")) {
            hasilku = (angka1 * angka2) - angka3;
        } else if (oper.equals(":")) {
            hasilku = angka1 / angka2;
            if (oper2.equals("+")) {
                hasilku += angka3;
            } else if (oper2.equals("-")) {
                hasilku -= angka3;
            }
        }

        pilihanRand(hasilku);
    }

    void pilihanRand(int hasil) {
        Random random = new Random();
        int pil2 = random.nextInt(100)+1;
        int pil3 = random.nextInt(50)+1;
        int pil4 = random.nextInt(20)+1;

        List<Integer> p = new ArrayList<Integer>();
        p.add(hasil);
        p.add(pil2);
        p.add(pil3);
        p.add(pil4);

        Collections.shuffle(p);

        pilihan1.setText(String.valueOf(p.get(0)));
        pilihan2.setText(String.valueOf(p.get(1)));
        pilihan3.setText(String.valueOf(p.get(2)));
        pilihan4.setText(String.valueOf(p.get(3)));
    }

    void nilai(int score) {
        this.score = score;
        total += score;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);

        skor = (TextView) findViewById(R.id.tvSkor);
        angkapertama = (TextView) findViewById(R.id.bil1);
        operat = (TextView) findViewById(R.id.op);
        angkakedua = (TextView) findViewById(R.id.bil2);
        operat2 = (TextView) findViewById(R.id.op2);
        angkaketiga = (TextView) findViewById(R.id.bil3);
        timer = (TextView) findViewById(R.id.timer);

        pilihan1 = (Button) findViewById(R.id.pilihan_1);
        pilihan2 = (Button) findViewById(R.id.pilihan_2);
        pilihan3 = (Button) findViewById(R.id.pilihan_3);
        pilihan4 = (Button) findViewById(R.id.pilihan_4);

        acak();

        countDownTimer = new MyCountDownTimer(startTime, interval);
        timer.setText("" + timer.getText() + String.valueOf(startTime / 1000));
        countDownTimer.start();
        timerStart = true;

        pilihan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sPil1 = pilihan1.getText().toString().trim();
                final int iPil1 = Integer.parseInt(sPil1);

                Log.d("TAG", "pil 1 "+iPil1);
                if (iPil1 == hasilku) {
                    nilai(10);
                    skor.setText(getString(R.string.text_skor_kamu_) + "" + total);
                    acak();
                } else {
                    Toast.makeText(getApplicationContext(), "Coba Lagi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pilihan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sPil2 = pilihan2.getText().toString().trim();
                final int iPil2 = Integer.parseInt(sPil2);

                Log.d("TAG", "pil 2 "+iPil2);
                if (iPil2 == hasilku) {
                    nilai(10);
                    skor.setText(getString(R.string.text_skor_kamu_) + "" + total);
                    acak();
                } else {
                    Toast.makeText(getApplicationContext(), "Coba Lagi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pilihan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sPil3 = pilihan3.getText().toString().trim();
                final int iPil3 = Integer.parseInt(sPil3);

                Log.d("TAG", "pil 3 "+iPil3);
                if (iPil3 == hasilku) {
                    nilai(10);
                    skor.setText(getString(R.string.text_skor_kamu_) + "" + total);
                    acak();
                } else {
                    Toast.makeText(getApplicationContext(), "Coba Lagi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pilihan4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sPil4 = pilihan4.getText().toString().trim();
                final int iPil4 = Integer.parseInt(sPil4);

                Log.d("TAG", "pil 4 "+iPil4);
                if (iPil4 == hasilku) {
                    nilai(10);
                    skor.setText(getString(R.string.text_skor_kamu_) + "" + total);
                    acak();
                } else {
                    Toast.makeText(getApplicationContext(), "Coba Lagi", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timer.setText("" + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            timer.setText("0");
            gameOver();
        }
    }

    public void gameOver() {
        dialog = new Dialog(Level3.this);
        dialog.setContentView(R.layout.dialog);
        TextView skorku = (TextView) dialog.findViewById(R.id.skorkuu);
        skorku.setText(String.valueOf(total));
        dialog.setTitle(R.string.text_game_over);
        dialog.setCancelable(true);
        dialog.show();

        Button btshare = (Button) dialog.findViewById(R.id.share);
        btshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TambahData();
                startActivity(new Intent(Level3.this, LihatData.class));
                dialog.dismiss();
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.btcancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });
    }

    private void TambahData() {
        EditText namaku = (EditText) dialog.findViewById(R.id.namakuu);
        TextView skorku1 = (TextView) dialog.findViewById(R.id.skorkuu);

        // Ubah setiap View EditText ke tipe Data String
        nama = namaku.getText().toString().trim();
        hasilskor = skorku1.getText().toString().trim();
        // Pembuatan Class AsyncTask yang berfungsi untuk koneksi ke Database Server

        class TambahData extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Level3.this, "Proses Kirim Data...", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(Level3.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String, String> params = new HashMap<>();
                // Sesuaikan bagian ini dengan field di tabel Mahasiswa
                params.put(config.KEY_EMP_NAMA, nama);
                params.put(config.KEY_EMP_SKOR, hasilskor);
                //params.put(config.KEY_EMP_JURUSAN,jurusan);

                RequestHandler rh = new RequestHandler();
                return rh.sendPostRequest(config.URL_ADD, params);
            }
        }
        // Jadikan Class TambahData Sabagai Object Baru
        TambahData ae = new TambahData();
        ae.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        countDownTimer.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        countDownTimer.cancel();
    }
}
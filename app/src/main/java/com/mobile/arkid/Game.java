package com.mobile.arkid;

import android.app.Dialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Game extends AppCompatActivity {
    private CountDownTimer countDownTimer;
    private boolean timerStart = true;
    private final long startTime=30*1000;
    private final long interval= 1*1000;
    Dialog dialog;

    TextView angkapertama, angkakedua, operat, timer, skor;
    EditText hasilnya;
    Button hitunghasil;

    String st_angka1, st_angka2, oper;
    int angka1, angka2, total=0, score, hasilku;

    void acak(){
        String opp[] ={"+","-","*"};
        int operator =(int) (Math.random()*3);
        oper = opp[operator];
        angka1 = (int) (Math.random() * 10) + 1;
        angka2 = (int) (Math.random() * 10) + 1;

        st_angka1 = String.valueOf(angka1);
        angkapertama.setText(st_angka1);

        operat.setText(oper);

        st_angka2 = String.valueOf(angka2);
        angkakedua.setText(st_angka2);
    }

    void nilai(int score){
        this.score = score;
        total+=score;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        skor = (TextView) findViewById(R.id.tvSkor);
        angkapertama = (TextView) findViewById(R.id.bil1);
        operat = (TextView) findViewById(R.id.op);
        angkakedua = (TextView) findViewById(R.id.bil2);
        hitunghasil = (Button) findViewById(R.id.btOk);
        hasilnya = (EditText) findViewById(R.id.etjawab);
        timer = (TextView) findViewById(R.id.timer);

        acak();

        countDownTimer=new MyCountDownTimer(startTime, interval);
        timer.setText(""+timer.getText()+ String.valueOf(startTime/1000));
        countDownTimer.start();
        timerStart=true;

        hitunghasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(oper.equals("+")){
                    hasilku = angka1 + angka2;
                }else if (oper.equals("-")){
                    hasilku = angka1 - angka2;
                }else if (oper.equals("*")){
                    hasilku = angka1 * angka2;
                }

                String hasil_1 = hasilnya.getText().toString();
                int hasil_2 = Integer.parseInt(hasil_1);

                if (hasil_2 == hasilku) {
                    Toast.makeText(getApplicationContext(), "Jawaban Benar", Toast.LENGTH_LONG).show();
                    nilai(10);
                    skor.setText(R.string.text_skor + total);
                    hasilnya.setText(null);
                    acak();
                } else {
                    Toast.makeText(getApplicationContext(), "Coba Lagi", Toast.LENGTH_LONG).show();
                    hasilnya.setText(null);
                }
            }
        });
    }

    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval){
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timer.setText(""+millisUntilFinished/1000);
        }

        @Override
        public void onFinish() {
            timer.setText("0");
            gameOver();
        }

    }

    public  void gameOver(){
        dialog= new Dialog(Game.this);
        dialog.setContentView(R.layout.dialog);
        TextView skorku = (TextView)findViewById(R.id.tvSkor);
        skorku.setText(R.string.text_skor+total);
        dialog.setTitle(R.string.text_game_over);
        dialog.setCancelable(false);
        dialog.show();


        Button btshare= (Button)dialog.findViewById(R.id.share);
        btshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Menu.class);
                startActivity(intent);
                //dialog.dismiss();
            }
        });
    }
}